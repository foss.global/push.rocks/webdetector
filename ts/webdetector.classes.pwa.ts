export class Pwa {
  public isInstalled(): boolean {
    let isInstalled: boolean;
    if (globalThis?.matchMedia?.('(display-mode: standalone)').matches) {
      isInstalled = true;
      console.log('PWA installed');
    } else {
      isInstalled = false;
    }
    return isInstalled;
  }
}