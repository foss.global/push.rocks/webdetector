import { tap, expect } from '@pushrocks/tapbundle';

import * as webdetector from '../ts/webdetector.classes.webdetector.js';
let testWebDetector: webdetector.WebDetector;

tap.test('first test', async () => {
  testWebDetector = new webdetector.WebDetector({
    checkOnlineUrl: 'https://pubapi.lossless.one',
  });

  expect(testWebDetector).toBeInstanceOf(webdetector.WebDetector);
});

tap.test('should be online', async () => {
  const onlineResultBoolean = await testWebDetector.isOnline();
  console.log('browser is online:');
  console.log(onlineResultBoolean);
  testWebDetector.startPeriodicChecks();
  testWebDetector.onlineObservable.subscribe((state) => {
    console.log(state);
  });
  testWebDetector.stopPeriodicChecks();
});

tap.test('should detect the platform', async () => {
  const platform = testWebDetector.platform.detectPlatform();
  console.log(platform);
});

tap.test('should detect if the app is installed', async () => {
  const isInstalled = testWebDetector.pwa.isInstalled();
  expect(isInstalled).toBeFalse();
});

console.log('hi');

tap.start();
